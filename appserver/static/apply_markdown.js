require([
    "/en-US/static/app/trunk/markdown.min.js",
    "splunkjs/mvc",
    "splunkjs/mvc/tableview",
    "splunkjs/mvc/simplexml/ready!"
], function(markdown, mvc, TableView) {

    var myCustomTable1 = splunkjs.mvc.Components.get("myevents1");
    var myCustomTable2 = splunkjs.mvc.Components.get("myevents2");
    var myCustomTable3 = splunkjs.mvc.Components.get("myevents3");
    var myCustomTable4 = splunkjs.mvc.Components.get("myevents4");

    var CustomCellRenderer = TableView.BaseCellRenderer.extend({ 
        canRender: function(cellData) {
            return cellData.field === 'Description' || cellData.field === 'Info';
        },
                     
        render: function($td, cellData) {
          if(!!cellData.value) {
            $td[0].innerHTML = window.markdown.toHTML(cellData.value);
          }
        }
    });

    var myCellRenderer = new CustomCellRenderer();
    
    myCustomTable1.getVisualization(function(tableView) {
        tableView.table.addCellRenderer(myCellRenderer); 
        tableView.render();
    });
    myCustomTable2.getVisualization(function(tableView) {
        tableView.table.addCellRenderer(myCellRenderer); 
        tableView.render();
    });
    myCustomTable3.getVisualization(function(tableView) {
        tableView.table.addCellRenderer(myCellRenderer); 
        tableView.render();
    });
    myCustomTable4.getVisualization(function(tableView) {
        tableView.table.addCellRenderer(myCellRenderer); 
        tableView.render();
    });
    
});
